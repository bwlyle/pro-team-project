from flask import Flask, jsonify, request
import os

app = Flask(__name__)

PORT = os.getenv("PORT")

#Sudo Database of Projects
projects = [
    {
        "userId": 1,
        "id": 1,
        "projectName": "DatacenterBuild",
        "companyName": "DataCenter"
    },
    {
        "userId": 2,
        "id": 2,
        "projectName": "PlaceToEatBuild",
        "companyName": "FoodPlace"
    },
    {
        "userId": 3,
        "id": 3,
        "projectName": "ProTeamForm",
        "companyName": "ProTeam",
        "productDescription" : "Awesome Sauce"
    },
    {
        "userId": 3,
        "id": 4,
        "projectName": "ProTeamFunction",
        "companyName": "ProTeam",
        "productDescription" : "Awesome Sauce"
    },
]

#Sudo Database of Vendors
vendors = [
    {
        "companyName": "Company1",
        "companyID": 4,
        "approvedAccountManager": "Random1",
        "emailAddress":	"vendor1@vender.com",
        "phoneNumber":	2145550000,
        "address": "1333 Anderson Dr.",
        "taxID": "int23345",
        "website": "vendor1.com",
    },
    {
        "companyName": "Company2",
        "companyID": 5,
        "approvedAccountManager": "Random2",
        "emailAddress":	"vendor2@vender.com",
        "phoneNumber":	2145550001,
        "address": "1334 Anderson Dr.",
        "taxID": "int33345",
        "website": "vendor2.com",
    },
    {
        "companyName": "Company3",
        "companyID": 6,
        "approvedAccountManager": "Random3",
        "emailAddress":	"vendor3@vender.com",
        "phoneNumber":	2145550002,
        "address": "1335 Anderson Dr.",
        "taxID": "int43345",
        "website": "vendor3.com",
    },
    {
        "companyName": "Company4",
        "companyID": 7,
        "approvedAccountManager": "Random4",
        "emailAddress":	"vendor4@vender.com",
        "phoneNumber":	2145550003,
        "address": "1336 Anderson Dr.",
        "taxID": "int53345",
        "website": "vendor4.com",
    },
]

#Sudo Database of Approvers
approvers = [
    {
        "approverName": "Madison",
        "approverID": "GFD23445",
        "approvalTitle" : "CEO",
        "approvalLimit": 1000000,
        "roleID": "The Boss",
        "approvalTime": 2,
    },
    {
        "approverName": "Sam",
        "approverID": "PSP1245",
        "approvalTitle" : "EVP",
        "approvalLimit": 500000,
        "roleID": "Next In Line",
        "approvalTime": 2,
    },
    {
        "approverName": "Jill",
        "approverID": "GFD23445",
        "approvalTitle" : "Director",
        "approvalLimit": 100000,
        "roleID": "Mover and Shaker",
        "approvalTime": 2,
    },
]
#Sudo Database of masterAgreements
masterAgreement = [
    {
	    "projectNumber": "1",
	    "productDescription": "Widgets 1",
    	"supplierCompanyName": "Vendor1",
	    "buyCompanyName": "Company1",
    	"maxBudget": "1000000",
	    "departmentCategory": "Consumables",
    	"contractID": "445676",
	    "departmentID": "234467",
    	"supplierID": "11111",
	    "startLifeSpan": "Today",
    	"renewalDate": "Tomorrow",
	    "reviewDate": "In the Future",
    	"buyerAddress": "4444 Now Ln.",
	    "supplierAddress": "5555 Here We Go Dr.",
    },
    {
    	"projectNumber": "1",
	    "productDescription": "Nic Nacs",
    	"supplierCompanyName": "Vendor2",
	    "buyCompanyName": "Company2",
    	"maxBudget": "1500000",
	    "departmentCategory": "Perishables",
    	"contractID": "88879",
	    "departmentID": "99099",
    	"supplierID": "11111",
	    "startLifeSpan": "Today",
    	"renewalDate": "Tomorrow",
	    "reviewDate": "In the Future",
    	"buyerAddress": "1111 Then Ln.",
	    "supplierAddress": "7777 UTD Dr.",
    },
    {
    	"projectNumber": "1",
	    "productDescription": "Nic Nacs",
    	"supplierCompanyName": "Vendor3",
	    "buyCompanyName": "Company3",
    	"maxBudget": "2000000",
	    "departmentCategory": "Reusables",
    	"contractID": "44479",
	    "departmentID": "899",
    	"supplierID": "12321",
	    "startLifeSpan": "Today",
    	"renewalDate": "Tomorrow",
	    "reviewDate": "In the Future",
    	"buyerAddress": "1456 Howedy Ln.",
	    "supplierAddress": "9876 University Dr.",
    },   
]
#Sudo Database of Buyers
buyers = [
    {
        "companyName": "Buyer1",
        "companyID": 1,
        "approvedUsername": "Rando1",
        "emailAddress":	"buyer1@buyer.com",
        "phoneNumber":	2143332222,
        "address": "122 UTD Crossing.",
        "taxID": "tax1",
    },
    {
        "companyName": "Buyer2",
        "companyID": 2,
        "approvedUsername": "Rando2",
        "emailAddress":	"buyer2@buyer.com",
        "phoneNumber":	2143332233,
        "address": "123 UTD Running.",
        "taxID": "tax2",
    },
    {
        "companyName": "Buyer3",
        "companyID": 3,
        "approvedUsername": "Random3",
        "emailAddress":	"buyer3@buyer.com",
        "phoneNumber":	2143332244,
        "address": "124 UTD Walking.",
        "taxID": "tax3",
    },
    {
        "companyName": "Buyer4",
        "companyID": 4,
        "approvedUsername": "Random4",
        "emailAddress":	"buyer1@buyer.com",
        "phoneNumber":	2143332255,
        "address": "123 UTD Crossing.",
        "taxID": "tax4",
    },
]

vendorProducts = [
    {
        "price": 1000,
        "productDescription": "The stuff you need",
        "picture":	"jpeg",
        "specs":    "matching",
        "partNumber":	"123SDFDS3",
        "sku": "sldjfsdljkf",
        "quantity": 1000,
    },
    {
        "price": 2000,
        "productDescription": "The stuff we have",
        "picture":	"jpeg",
        "specs":    "matching",
        "partNumber":	"PPo0988",
        "sku": "12312jsldjf",
        "quantity": 2000,
    },
    {
        "price": 3000,
        "productDescription": "Just Stuff",
        "picture":	"jpeg",
        "specs":    "matching",
        "partNumber":	"LIUL78",
        "sku": "098098",
        "quantity": 3000,
    },
    {
        "price": 4000,
        "productDescription": "Things",
        "picture":	"jpeg",
        "specs":    "matching",
        "partNumber":	"12312",
        "sku": "LJLKJLJ",
        "quantity": 4000,
    },
]

#Instantiating Several Element Lists
requisitions = []
purchaseOrders = []
quotes = []
payment = []
paymentInvoices = []

#Get All Requisitions
@app.route("/requisitions/", methods=["GET"])
def get_requisitions():
    return jsonify(requisitions)

#Get All Projects
@app.route("/projects/", methods=["GET"])
def get_projects():
    return jsonify(projects)

#Get All Approvers
@app.route("/approvers/", methods=["GET"])
def get_approvers():
    return jsonify(approvers)

#Get All Vendors
@app.route("/vendors/", methods=["GET"])
def get_vendors():
    return jsonify(vendors)

#Get All Vendors Products
@app.route("/vendorProducts/", methods=["GET"])
def get_vendorProducts():
    return jsonify(vendorProducts)

#Get All Master Agreements
@app.route("/masterAgreement/", methods=["GET"])
def get_master_agreement():
    return jsonify(masterAgreement)

#Get Buyers
@app.route("/buyers/", methods=["GET"])
def get_buyers():
    return jsonify(buyers)

#Get Quotes
@app.route("/quotes/", methods=["GET"])
def get_quotes():
    return jsonify(quotes)

#Get PurchaseOrder
@app.route("/purchaseOrders/", methods=["GET"])
def get_purchaseOrder():
    return jsonify(purchaseOrders)

#Get Purchase Order for company
@app.route("/purchaseOrders/<customerName>", methods=["GET"])
def get_purchaseOrderForCompany(customerName):
    return jsonify(purchaseOrders)

#Get payment and invoice
@app.route("/paymentInvoice/", methods=["GET"])
def get_invoice():
    return jsonify(paymentInvoices)

#Get Payment and Invoice for poID
@app.route("/paymentInvoice/<poID>", methods=["GET"])
def get_paymentInvoice(poID):
    poID = int(poID)
    for invoice in paymentInvoices:
        print(invoice)
        if invoice["poID"] == poID:
            return jsonify(invoice)

# http://localhost:5000/requester/1
@app.route("/requester/<userId>", methods=["GET"])
def get_requester(userId):
    # algorithm
    userId = int(userId)

    return jsonify({"userName": "Pro Team", "userId": userId})

#Get Projects for an individual UserID
@app.route("/requester/<userId>/projects", methods=["GET"])
def get_projects_for_user(userId):

    # algorithm
    userId = int(userId)
    user_projects = []
    for project in projects:
        if project["userId"] == userId:
            user_projects.append(project)

    return jsonify(user_projects)

#Create Requisitions
@app.route("/requisitions", methods=["POST"])
def create_new_requisition():
    body = request.json

    print(body)

    requisitions.append(body)
    return jsonify({"status": "ok"})

#Create Master Agreement
@app.route("/masterAgreement", methods=["POST"])
def create_master_agreement():
    body = request.json

    for req in requisitions:
        req.update(body)
    return jsonify({"status": "ok"})

#Approve Requisition (update Requisition buy title/amount)
@app.route("/requisitions/<approverTitle>/approved", methods=["POST"])
def req_approved(approverTitle):
    body = request.json

    for req in requisitions:
        if req["approvalAmount"] <= 100000 and approverTitle == "Director":
            req.update(body)
        elif req["approvalAmount"] <= 500000 and approverTitle == "EVP":
            req.update(body)
        elif req["approvalAmount"] >= 1000000 and approverTitle == "CEO":
            req.update(body)
    return jsonify({"status": "ok"})

#Deny Requisition (Update Requisition)
@app.route("/requisitions/<approverTitle>/denied", methods=["POST"])
def req_denied(approverTitle):
    body = request.json

    for req in requisitions:
        req.update(body)
        
    return jsonify({"status": "ok"})

#Update Requistion with MasterAgreement ContractID
@app.route("/requisitions/<reqId>/masterAgreement", methods=["POST"])
def update_req_with_contractID(reqId):
    body = request.json

    for req in requisitions:
        req.update(body)
    return jsonify({"status": "ok"})

#Buyer Gets the Req
@app.route("/requistions/<buyerId>/", methods=["GET"])
def get_req_for_buyer(buyerId):

    # algorithm
    userId = int(userId)
    user_projects = []
    for project in projects:
        if buyer["userId"] == userId:
            user_projects.append(project)

    return jsonify(user_projects)

#Post Quotes
@app.route("/quotes/", methods=["POST"])
def create_quote():
    body = request.json

    print(body)

    quotes.append(body)
    return jsonify({"status": "ok"})

#Post Purchase Order
@app.route("/purchaseOrders/<customerName>", methods=["POST"])
def create_purchaseOrders(customerName):
    body = request.json
    purchaseOrder = {}
    print(body)
    for quote in quotes:
        if quote["customerName"] == customerName:
            purchaseOrder["price"] = quote["price"]
            purchaseOrder["productDescription"] = quote["productDescription"]
            purchaseOrder["specs"] =	quote["specs"]
            purchaseOrder["partNumber"] =	quote["partNumber"]
            purchaseOrder["sku"] =	quote["sku"]
            purchaseOrder["quantity"] = quote["quantity"]
            purchaseOrder["requesterName"] = quote["requesterName"]
            purchaseOrder["quoteID"] = quote["quoteID"]
            purchaseOrder["quoteDate"] = quote["quoteDate"]
            purchaseOrder["experationDate"] = quote["experationDate"]
            purchaseOrder["companyID"] = quote["companyID"]
            purchaseOrder["shipToAddress"] = quote["shipToAddress"]
            purchaseOrder["billToAddress"] = quote["billToAddress"]
            purchaseOrder["customerName"] = quote["customerName"]
            purchaseOrder["discountInformation"] = quote["discountInformation"]
            purchaseOrder["buyerName"] = quote["buyerName"]
            purchaseOrder["requesterName"] = quote["requesterName"]
            purchaseOrder["customerName"] = quote["customerName"]
            purchaseOrder["poID"] = 1234

    purchaseOrders.append(purchaseOrder)
    return jsonify({"status": "ok"})

#Vendor Update Purchase Order
@app.route("/payment/filled", methods=["POST"])
def payment_fileed():
    body = request.json

    for req in requisitions:
        req.update(body)
        
    return jsonify({"status": "ok"})

#Create an Invoice
@app.route("/paymentInvoice/<poID>", methods=["POST"])
def create_paymentInvoice(poID):
    body = request.json
    invoice = {}
    for purchaseOrder in purchaseOrders:
        if purchaseOrder["poID"] == int(poID):
            print("In the IF statement")
            invoice["price"] = purchaseOrder["price"]
            invoice["productDescription"] = purchaseOrder["productDescription"]
            invoice["partNumber"] = purchaseOrder["partNumber"]
            invoice["sku"] = purchaseOrder["sku"]
            invoice["quantity"] = purchaseOrder["quantity"]
            invoice["poID"] = purchaseOrder["poID"]
            invoice["invoiceDate"] = "Today"
            invoice["paymentTerms"] = "All the money"
            invoice["companyID"] = purchaseOrder["companyID"]
            invoice["buyerName"] = purchaseOrder["buyerName"]
            invoice["customerName"] = purchaseOrder["customerName"]
            invoice["shipToAddress"] = purchaseOrder["shipToAddress"]
            invoice["billToAddress"] = purchaseOrder["billToAddress"]
            invoice["customerName"] = purchaseOrder["customerName"]
            invoice["PO Status"] = ""
           
    paymentInvoices.append(invoice)
    return jsonify({"status": "ok"})

#Payment Invoice Approved
@app.route("/paymentInvoice/approved", methods=["POST"])
def req_paymentInvoice_approved():
    body = request.json

    for invoice in paymentInvoices:
        invoice.update(body)
        
    return jsonify({"status": "ok"})

#Payment Invoice Denied
@app.route("/paymentInvoice/approved", methods=["POST"])
def req_paymentInvoice_denied():
    body = request.json

    for invoice in paymentInvoices:
        invoice.update(body)
        
    return jsonify({"status": "ok"})

#Payment Status Filled
@app.route("/paymentstatus/received", methods=["POST"])
def req_paymentStatis_received():
    body = request.json

    for invoice in paymentInvoices:
        invoice.update(body)
        
    return jsonify({"status": "ok"})

if __name__ == "__main__":
    app.run(debug=True, port=PORT, host="0.0.0.0")
